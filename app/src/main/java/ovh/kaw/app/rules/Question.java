package ovh.kaw.app.rules;


import java.util.ArrayList;

import ovh.kaw.app.enums.Color;
import ovh.kaw.app.enums.RuleType;

public class Question extends Rule {

    private ArrayList<Answer> answers;

    public Question(RuleType type, String message, int sips, boolean cibled, int level, int minPlayers, boolean applyEffects, ArrayList<Effect> effects, ArrayList<Answer> answers) {
        super(type, message, sips, cibled, level, minPlayers, applyEffects, effects);
        this.answers = answers;
    }

    public Question(Question question){
        super(question.getType(), question.getMessage(), question.getSips(), question.isCibled(), question.getLevel(), question.getMinPlayers(), question.isApplyEffects(), question.getEffects());
        this.answers = question.getAnswers();
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }
}
