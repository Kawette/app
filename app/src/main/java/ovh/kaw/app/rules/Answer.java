package ovh.kaw.app.rules;


import java.util.ArrayList;

import ovh.kaw.app.enums.Color;

public class Answer {

    private String answer;
    private String message;
    private Color color;
    private ArrayList<Effect> effects;

    public Answer(String answer, String message, Color color, ArrayList<Effect> effects) {
        this.answer = answer;
        this.message = message;
        this.color = color;
        this.effects = effects;
    }

    public String getAnswer() {
        return answer;
    }

    public ArrayList<Effect> getEffects() {
        return effects;
    }

    public Color getColor() {
        return color;
    }
}
