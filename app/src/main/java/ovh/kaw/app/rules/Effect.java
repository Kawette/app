package ovh.kaw.app.rules;


import ovh.kaw.app.enums.EffectType;

public class Effect {

    private EffectType type;
    private String value;
    private int duration;
    private boolean global;
    private String player;

    public Effect(EffectType effectType, String value, int duration, Boolean global, String player){
        this.type = effectType;
        this.value = value;
        this.duration = duration;
        this.player = player;
        this.global = global;
    }

    public Effect(Effect effect) {
        this(effect.getType(), effect.getStringValue(), effect.getDuration(), effect.isGlobal(), effect.getPlayer());
    }

    public EffectType getType() {
        return type;
    }

    public int getIntValue(){
        return Integer.valueOf(value);
    }

    public String getStringValue(){
        return value;
    }

    public boolean isGlobal() {
        return global;
    }

    public String getPlayer() {
        return player;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
