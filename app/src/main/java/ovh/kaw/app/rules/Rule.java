package ovh.kaw.app.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import ovh.kaw.app.core.Player;
import ovh.kaw.app.enums.RuleType;

public class Rule implements Cloneable{

    private RuleType type;
    private String message;
    private int sips;
    private boolean cibled;
    private int level;
    private int minPlayers;
    private boolean applyEffects;
    private ArrayList<Effect> effects;

    private Player cibledPlayer;
    private ArrayList<Player> randomPlayers = new ArrayList<>();

    public Rule(RuleType type, String message, int sips, boolean cibled, int level, int minPlayers, boolean applyEffects, ArrayList<Effect> effects) {
        this.type = type;
        this.sips = sips;
        this.message = message;
        this.cibled = cibled;
        this.level = level;
        this.minPlayers = minPlayers;
        this.effects = effects;
        this.applyEffects = applyEffects;
        setCibledPlayer(null);
    }

    // Clone
    public Rule(Rule rule) {
        this(rule.getType(), rule.getMessage(), rule.getSips(), rule.isCibled(), rule.getLevel(), rule.getMinPlayers(), rule.isApplyEffects(), rule.getEffects());
    }

    public void exec(HashMap<String, Player> players, int drinkLevel, Player cibledPlayer){

        ArrayList<String> keys = new ArrayList<>(players.keySet());

        // Set cibled player (only for cibled rule)
        if(isCibled()){
            setCibledPlayer(cibledPlayer);
        }

        // Setup random players (%P1%, %P2%, %P3%, %P4%)
        Player randomPlayer;
        if(isCibled()) keys.remove(cibledPlayer.getName());
        for(int i = 0; i < 4; i++){
            if(keys.size() <= 0)
                break;
            randomPlayer = players.get(keys.remove(new Random().nextInt(keys.size())));
            randomPlayers.add(randomPlayer);
        }

        // Adapt sips
        int dl;
        if(cibled)
            dl = cibledPlayer.getDrinkLevel();
        else
            dl = drinkLevel;
        int sips = this.sips + dl / 10 - 5;
        this.sips = sips <= 0 ? 1 : sips;

    }

    public String getParsedMessage() {
        return parse(message);
    }

    public String getMessage(){
        return message;
    }

    public String parse(String str) {
        if(cibled)
            str = str.replaceAll("%C%", cibledPlayer.getName());
        str = str.replaceAll("%S%", String.valueOf(getSips()));
        for(int i = 0; i < randomPlayers.size(); i++){
            str = str.replaceAll("%P" + (i + 1) + "%", randomPlayers.get(i).getName());
        }
        return str;
    }

    public boolean isCibled() {
        return cibled;
    }

    public void setCibledPlayer(Player player) {
        cibledPlayer = player;
    }

    public int getSips() {
        return sips;
    }

    public int getLevel() {
        return level;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public ArrayList<Effect> getEffects() {
        return effects;
    }

    public Player getCibledPlayer() {
        return cibledPlayer;
    }

    public RuleType getType() {
        return type;
    }

    public void setSips(int sips) {
        this.sips = sips;
    }

    public boolean isApplyEffects() {
        return applyEffects;
    }

}
