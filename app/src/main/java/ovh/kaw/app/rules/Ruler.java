package ovh.kaw.app.rules;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ovh.kaw.app.enums.Color;
import ovh.kaw.app.enums.EffectType;
import ovh.kaw.app.enums.RuleType;

import static ovh.kaw.app.enums.RuleType.*;

public class Ruler {

    private static final boolean DEFAULT_RULE_CIBLED = false;
    private static final int     DEFAULT_RULE_LEVEL = 0;
    private static final int     DEFAULT_RULE_SIPS = 0;
    private static final int     DEFAULT_RULE_MIN_PLAYER = 0;

    private static final boolean DEFAULT_SIMPLE_APPLY_EFFECTS = true;

    private static final boolean DEFAULT_QUESTION_APPLY_EFFECTS = false;

    private static final int     DEFAULT_EFFECT_DURATION = 1;
    private static final Boolean DEFAULT_EFFECT_GLOBAL = false;

    private ArrayList<Rule> rules = new ArrayList<>();

    public void initRules(InputStream in) {

        ArrayList<Rule> rules = new ArrayList<>();

        try {
            JSONArray data = new JSONArray(streamToString(in));

            // For each rule
            for(int i = 0; i < data.length(); i++){
                JSONObject rule = data.getJSONObject(i);

                RuleType type = valueOf(rule.getString("type"));

                // Optional params (global)
                int     sips         = rule.has("sips")         ? rule.getInt("sips")       : DEFAULT_RULE_SIPS;
                boolean cibled       = rule.has("cibled")       ? rule.getBoolean("cibled") : DEFAULT_RULE_CIBLED;
                int     level        = rule.has("level")        ? rule.getInt("level")      : DEFAULT_RULE_LEVEL;
                int     minPlayers   = rule.has("minPlayers")   ? rule.getInt("minPlayers") : DEFAULT_RULE_MIN_PLAYER;
                ArrayList<Effect> effects = rule.has("effects") ? initEffects(rule.getJSONArray("effects")) : new ArrayList<>();

                // Specific optional params
                boolean applyEffects;

                switch (type){
                    case SIMPLE:
                        applyEffects = rule.has("applyEffects") ? rule.getBoolean("applyEffects") : DEFAULT_SIMPLE_APPLY_EFFECTS;
                        rules.add(new Rule(SIMPLE, rule.getString("message"), sips, cibled, level, minPlayers, applyEffects, effects));
                        break;
                    case QUESTION:
                        applyEffects = rule.has("applyEffects") ? rule.getBoolean("applyEffects") : DEFAULT_QUESTION_APPLY_EFFECTS;
                        ArrayList<Answer> answers = rule.has("answers") ? initAnswers(rule.getJSONArray("answers")) : new ArrayList<>();
                        rules.add(new Question(QUESTION, rule.getString("message"), sips, cibled, level, minPlayers, applyEffects, effects, answers));
                        break;
                    default:
                        System.out.println("Invalid type for rule : " + i + " Ignored");
                        break;
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        setRules(rules);

    }

    private ArrayList<Answer> initAnswers(JSONArray data) {

        ArrayList<Answer> answers = new ArrayList<>();

        // For each answers
        for(int i = 0; i < data.length(); i++){
            try {
                JSONObject answer = data.getJSONObject(i);

                String message            = answer.has("message") ? answer.getString("message")                 : null;
                Color color               = answer.has("color")   ? Color.valueOf(answer.getString("color"))    : Color.BLUE;
                ArrayList<Effect> effects = answer.has("effects") ? initEffects(answer.getJSONArray("effects")) : new ArrayList<>();
                answers.add(new Answer(answer.getString("answer"), message, color, effects));
            } catch (JSONException e){
                e.printStackTrace();
                System.exit(1);
            }
        }
        return answers;
    }

    private ArrayList<Effect> initEffects(JSONArray data) {

        ArrayList<Effect> effects = new ArrayList<>();

        // For each effect
        for(int i = 0; i < data.length(); i++){
            try {
                JSONObject effect = data.getJSONObject(i);
                int duration   = effect.has("duration") ? effect.getInt("duration")   : DEFAULT_EFFECT_DURATION;
                String player  = effect.has("player")   ? effect.getString("player")  : null;
                Boolean global = effect.has("global")   ? effect.getBoolean("global") : DEFAULT_EFFECT_GLOBAL;
                effects.add(new Effect(EffectType.valueOf(effect.getString("type")), effect.getString("value"), duration, global, player));
            } catch (JSONException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return effects;
    }

    private String streamToString(InputStream inputStream) {
        BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder sb =  new StringBuilder();
        try {
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public void setRules(ArrayList<Rule> rules) {
        this.rules = rules;
    }

    public ArrayList<Rule> getRules(){
        return rules;
    }

}
