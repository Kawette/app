package ovh.kaw.app.activities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import ovh.kaw.app.R;
import ovh.kaw.app.App;
import ovh.kaw.app.core.Game;
import ovh.kaw.app.enums.RuleType;
import ovh.kaw.app.rules.Answer;
import ovh.kaw.app.rules.Question;
import ovh.kaw.app.rules.Rule;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    private Game game;

    private TextView messageTextView;
    private Button buttonNext;
    private ArrayList<Button> buttons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Lock landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        // Loading game engine
        game = ((App) this.getApplication()).getGame();

        setContentView(R.layout.activity_game);

        // Loading component
        messageTextView = findViewById(R.id.gameMessage);
        buttonNext = findViewById(R.id.gameButtonNext);
        buttons.add(findViewById(R.id.gameButton1));
        buttons.add(findViewById(R.id.gameButton2));
        buttons.add(findViewById(R.id.gameButton3));
        buttons.add(findViewById(R.id.gameButton4));

        // Setup component
        initRule(game.getCurrentRule());

    }

    private void initRule(Rule rule) {

        // Reset buttons
        buttonNext.setVisibility(View.INVISIBLE);
        for(Button b : buttons){
            b.setVisibility(View.INVISIBLE);
            b.setOnClickListener(null);
        }

        messageTextView.setText(rule.getParsedMessage());
        switch (rule.getType()){
            case SIMPLE:
                buttonNext.setVisibility(View.VISIBLE);
                break;
            case QUESTION:
                Question question = (Question) rule;
                ArrayList<Answer> answers = question.getAnswers();
                int start = 0, end = 4;
                if(answers.size() == 2) {
                    start = 1;
                    end = 2;
                }
                else if (answers.size() == 4) {
                    start = 0;
                    end = 3;
                }
                else {
                    System.err.println("Only 2 or 4 answers");
                    System.exit(1);
                }
                for(int i = start; i <= end; i++){
                    Button button = buttons.get(i);
                    Answer answer = answers.get(i - start);

                    button.setVisibility(View.VISIBLE);

                    // Button text
                    button.setText(question.parse(answer.getAnswer()));

                    // Button color
                    button.getBackground().setColorFilter(getResources().getColor(answer.getColor().getResource()), PorterDuff.Mode.MULTIPLY);

                    // OnClickListener
                    int finalI = i;
                    int finalStart = start;
                    buttons.get(i).setOnClickListener(v -> {
                        // Dispatch answer effects
                        game.dispatchEffects(answers.get(finalI - finalStart).getEffects());
                        onClickNext(v);
                    });
                }
                break;
        }
    }

    public void onClickNext(View view){
        if(!game.isEnded()) {
            game.nextRule();
            initRule(game.getCurrentRule());
        }
        else {
            game.endGame();
            finish();
        }
    }

    @Override
    public void onClick(View view) {}

}
