package ovh.kaw.app.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import ovh.kaw.app.R;
import ovh.kaw.app.App;
import ovh.kaw.app.core.Game;

public class MainActivity extends AppCompatActivity {

    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Loading game engine
        game = ((App) this.getApplication()).getGame();
        game.initGame(getResources().openRawResource(R.raw.rules));

        setContentView(R.layout.activity_main);
    }

    public void onClickPlay(View view) {
        game.startGame();
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }
}
