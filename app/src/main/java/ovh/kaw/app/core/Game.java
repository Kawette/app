package ovh.kaw.app.core;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import ovh.kaw.app.rules.Effect;
import ovh.kaw.app.enums.EffectType;
import ovh.kaw.app.rules.Question;
import ovh.kaw.app.rules.Rule;
import ovh.kaw.app.rules.Ruler;

public class Game {

    private static final int DEFAULT_GAME_LEVEL = 30;
    private static final int DEFAULT_DRINK_LEVEL = 40;

    private Ruler ruler;                                       // Rule engine
    private HashMap<String, Player> players = new HashMap<>(); // Players list
    private ArrayList<Rule> rules = new ArrayList<>();         // Rules list
    private Rule currentRule;                                  // Current rule
    private int gameLevel;                                     // Game level 0 to 100
    private int drinkLevel;                                    // Drink level 0 to 100
    private int turn;                                          // Game loop
    private ArrayList<Effect> effects = new ArrayList<>();     // Effects triggered

    public void initGame(InputStream dataFile) {

        // Setup rules
        ruler = new Ruler();
        ruler.initRules(dataFile);

        turn = 0; // Reset game loop

    }

    public void startGame(){
        // Reset game loop
        turn = 0;

        // Reset effects
        effects.clear();
        for(Map.Entry<String, Player> player : players.entrySet()){
            player.getValue().clearEffects();
        }

        // Build rules
        buildRules();

        // Setup global drink / game level
        setGameLevel(100);
        setDrinkLevel(DEFAULT_DRINK_LEVEL);

        // Adding players
        players.put("Christo", new Player("Christo", gameLevel, drinkLevel));
        players.put("Laura", new Player("Laura", gameLevel, drinkLevel));
        players.put("Marine", new Player("Marine", gameLevel, drinkLevel));
        players.put("Ben", new Player("Ben", gameLevel, drinkLevel));
        players.put("Yolo5", new Player("Yolo5", gameLevel, drinkLevel));

        // Shuffle
        Collections.shuffle(rules);

        nextRule();
    }

    public void nextRule(){
        // Remove current rule
        rules.remove(currentRule);

        // Game loop
        if(turn >= 9) // DEBUG
            turn = -1;
        else
            turn++;

        // No more rules ? Rebuild all rules
        if(rules.size() == 0)
            buildRules();

        // Find rule
        currentRule = null; // Reset current rule
        ArrayList<String> keys = new ArrayList<>(players.keySet());
        Player cibledPlayer = null;
        int extraGameLevel = 0;
        while(currentRule == null) { // Rule found ?
            // Find a correct rule (check for minPlayers and rule level)
            for (Rule rule : rules) {
                cibledPlayer = null;
                int gl; // GameLevel filter
                if (rule.isCibled()) { // CibledPlayer game level
                    cibledPlayer = players.get(keys.get(new Random().nextInt(keys.size())));
                    gl = cibledPlayer.getGameLevel();
                }
                else // Global game level
                    gl = gameLevel;

                if (rule.getLevel() <= gl + extraGameLevel && players.size() >= rule.getMinPlayers()) {
                    currentRule = rule;
                    break;
                }
            }
            // Not found ?
            if(currentRule == null){
                // Give +5 extra to game level and search again (max +10 extra)
                if(extraGameLevel < 10){
                    extraGameLevel += 5;
                }
                // Extra max reached ?
                else {
                    buildRules(); // Rebuild all rules
                }
            }
        }

        // Exec rule
        currentRule.exec(players, drinkLevel, cibledPlayer);

        // Apply effects
        if(currentRule.isApplyEffects()) { // Apply only if applyEffects = true
            if (currentRule.isCibled())
                applyEffects(currentRule.getCibledPlayer().getEffects());
            else
                applyEffects(this.effects);
        }

        // Add effects to his target
        dispatchEffects(currentRule.getEffects());
    }

    public void dispatchEffects(ArrayList<Effect> effects) {
        for (Effect effect : effects) {

            // Copy
            effect = new Effect(effect);

            // Instant effect ? DRINK_LEVEL, GAME_LEVEL
            if (effect.getType() == EffectType.DRINK_LEVEL || effect.getType() == EffectType.GAME_LEVEL) {
                // Apply and remove the effect
                applyEffect(effect);
            }
            // Other effect ? then add to target
            else {
                // Global effect ? (global = true)
                if (effect.isGlobal()) {
                    effects.size();
                    this.addEffect(effect); // Add effect to the game
                }
                // Special player ?
                else if (effect.getPlayer() != null) {
                    // Add effect to the special player
                    players.get(currentRule.parse(effect.getPlayer())).addEffect(effect);
                }
                // Cibled player
                else if (currentRule.isCibled()) {
                    // Add effect to the cibled player
                    currentRule.getCibledPlayer().addEffect(effect);
                } else {
                    System.err.println("Rule specification error : " + currentRule.getParsedMessage());
                    System.exit(1);
                }
            }
        }
    }

    private void applyEffect(Effect effect){
        ArrayList<Effect> effects = new ArrayList<>();
        effects.add(effect);
        applyEffects(effects);
    }

    private void applyEffects(ArrayList<Effect> effects) {
        for(Iterator<Effect> it = effects.iterator(); it.hasNext();){
            Effect effect = it.next();

            switch (effect.getType()){
                case SIPS:
                    currentRule.setSips(currentRule.getSips() + effect.getIntValue());
                    break;
                case DRINK_LEVEL:
                    if(effect.isGlobal())
                        drinkLevel += effect.getIntValue();
                    else if (effect.getPlayer() != null){
                        players.get(currentRule.parse(effect.getPlayer())).addDrinkLevel(effect.getIntValue());
                    }
                    else if (currentRule.isCibled() && effect.getPlayer() == null){
                        currentRule.getCibledPlayer().addDrinkLevel(effect.getIntValue());
                    }
                    break;
                case GAME_LEVEL:
                    if(effect.isGlobal())
                        gameLevel += effect.getIntValue();
                    else if (effect.getPlayer() != null){
                        players.get(currentRule.parse(effect.getPlayer())).addGameLevel(effect.getIntValue());
                    }
                    else if (currentRule.isCibled() && effect.getPlayer() == null){
                        currentRule.getCibledPlayer().addGameLevel(effect.getIntValue());
                    }
                    break;
            }

            effect.setDuration(effect.getDuration() - 1);
            if(effect.getDuration() <= 0){
                it.remove();
            }
        }
    }

    private void buildRules(){
        rules = new ArrayList<>();
        for(Rule rule : ruler.getRules()){
            switch (rule.getType()){
                case SIMPLE:
                    rules.add(new Rule(rule));
                    break;
                case QUESTION:
                    rules.add(new Question((Question) rule));
                    break;
                default:
                    System.err.println("Err clone in buildRules : type not handled");
                    System.exit(1);
            }
        }
    }

    private void addEffect(Effect effect){
        this.effects.add(effect);
    }

    public void endGame() {
        players.clear();
    }

    public boolean isEnded(){
        return turn == -1;
    }

    public Rule getCurrentRule() {
        return currentRule;
    }

    public void setGameLevel(int gameLevel) {
        // Game level 0 - 100
        if(gameLevel <= 100 && gameLevel >= 0)
            this.gameLevel = gameLevel;
    }

    public void setDrinkLevel(int drinkLevel) {
        // Drink level 0 - 100
        if(drinkLevel <= 100 && drinkLevel >= 0)
            this.drinkLevel = drinkLevel;
    }

}
