package ovh.kaw.app.core;

import java.util.ArrayList;

import ovh.kaw.app.rules.Effect;

public class Player {

    private String name;
    private int gameLevel;
    private int drinkLevel;
    private ArrayList<Effect> effects = new ArrayList<>();

    public Player(String name, int gameLevel, int drinkLevel) {
        setName(name);
        setDrinkLevel(drinkLevel);
        setGameLevel(gameLevel);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setGameLevel(int gameLevel) {
        // Game level 0 - 100
        if(gameLevel <= 100 && gameLevel >= 0)
            this.gameLevel = gameLevel;
    }

    public void setDrinkLevel(int drinkLevel) {
        // Drink level 0 - 100
        if(drinkLevel <= 100 && drinkLevel >= 0)
            this.drinkLevel = drinkLevel;
    }

    public int getDrinkLevel() {
        return drinkLevel;
    }

    public void addEffect(Effect effect) {
        effects.add(effect);
    }

    public ArrayList<Effect> getEffects() {
        return effects;
    }

    public void clearEffects() {
        effects.clear();
    }

    public void addDrinkLevel(int intValue) {
        drinkLevel += intValue;
    }

    public void addGameLevel(int intValue) {
        gameLevel += intValue;
    }

    public int getGameLevel() {
        return gameLevel;
    }
}
