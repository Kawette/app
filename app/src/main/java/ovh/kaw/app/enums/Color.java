package ovh.kaw.app.enums;


import ovh.kaw.app.R;

public enum Color {

    RED(R.color.colorRed),
    GREEN(R.color.colorGreen),
    BLUE(R.color.colorPrimary),
    YELLOW(R.color.colorYellow);

    private int resource;



    Color(int color) {
        resource = color;
    }

    public int getResource() {
        return resource;
    }
}
