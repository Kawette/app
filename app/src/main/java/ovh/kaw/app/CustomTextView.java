package ovh.kaw.app;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.util.LruCache;

import java.lang.reflect.Type;

import ovh.kaw.app.R;

public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    private static LruCache<String, Typeface> TYPEFACE_CACHE = new LruCache<String, Typeface>(12);

    public CustomTextView(Context context, AttributeSet attrs){
        super(context, attrs);

        if(isInEditMode()){
            return;
        }

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String fontName = styledAttrs.getString(R.styleable.CustomTextView_customFont);
        styledAttrs.recycle();
        setTypeFace(fontName);
    }

    public void setTypeFace(String fontName) {
        if(fontName != null){
            try {
                Typeface typeface = TYPEFACE_CACHE.get(fontName);
                if (typeface == null) {
                    typeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/" + fontName);
                    TYPEFACE_CACHE.put(fontName, typeface);
                }
                setTypeface(typeface);
            } catch (Exception e) {
                Log.e("FONT", fontName + " not found", e);
            }
        }
    }
}
