package ovh.kaw.app;


import android.app.Application;

import ovh.kaw.app.core.Game;

public class App extends Application {

    private Game game = new Game();

    public Game getGame() {
        return game;
    }
}
